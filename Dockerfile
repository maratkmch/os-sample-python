FROM python:3.6

COPY . .

CMD [ "python", "./wsgi.py" ]